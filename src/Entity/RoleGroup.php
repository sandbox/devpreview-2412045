<?php

/**
 * @file
 * Contains Drupal\role_group\Entity\RoleGroup.
 */

namespace Drupal\role_group\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\role_group\RoleGroupInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\user\RoleInterface;

/**
 * Defines the user role group entity class.
 *
 * @ConfigEntityType(
 *   id = "role_group",
 *   label = @Translation("Role groups"),
 *   handlers = {
 *     "list_builder" = "Drupal\role_group\RoleGroupListBuilder",
 *     "form" = {
 *       "default" = "Drupal\role_group\RoleGroupForm",
 *       "delete"  = "Drupal\role_group\RoleGroupDeleteForm",
 *       "roles"   = "Drupal\role_group\GroupRolesForm"
 *     }
 *   },
 *   admin_permission = "administer role groups",
 *   config_prefix = "role_group",
 *   static_cache = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "weight" = "weight",
 *     "label" = "label"
 *   },
 *   links = {
 *     "collection" = "/admin/people/role_groups",
 *     "edit-form" = "/admin/people/role_groups/manage/{role_group}",
 *     "edit-roles-form" = "/admin/people/role_groups/manage/{role_group}/roles",
 *     "delete-form" = "/admin/people/role_groups/manage/{role_group}/delete"
 *   }
 * )
 */
class RoleGroup extends ConfigEntityBase implements RoleGroupInterface {

  /**
   * The machine name of this role group.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of this role group.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of this role in administrative listings.
   *
   * @var int
   */
  protected $weight;

  /**
   * The roles of this role group.
   *
   * @var array
   */
  protected $roles = array();

  /**
   * The roles of this role group.
   *
   * @var RoleInterface[]
   */
  private $roleObjects = array();

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addRole($role) {
    if ($role instanceof RoleInterface) {
      if (!$this->hasRole($role->id())) {
        $this->roleObjects[$role->id()] = $role;
        $this->roles[] = $role->id();
      }
    }
    else if (!$this->hasRole($role->id())) {
      $this->roles[] = $role;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRole($role) {
    if ($role instanceof RoleInterface) {
      return in_array($role->id(), $this->roles);
    }
    else {
      return in_array($role, $this->roles);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delRole($role) {
    $role_id = $role;
    if ($role instanceof RoleInterface) {
      $role_id = $role->id();
    }
    if ($this->hasRole($role)) {
      unset($this->roles[$role_id]);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles() {
    $roles = array();
    foreach ($this->roles as $role_id) {
      if (!array_key_exists($role_id, $this->roleObjects)) {
        $this->roleObjects[$role_id] = entity_load('user_role', $role_id);
      }
      $roles[$role_id] = $this->roleObjects[$role_id];
    }
    return $roles;
  }

  /**
   * {@inheritdoc}
   */
  public function getRolesId() {
    return $this->roles;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    parent::postLoad($storage, $entities);
// Sort the queried role groups by their weight.
// See \Drupal\Core\Config\Entity\ConfigEntityBase::sort().
    uasort($entities, 'static::sort');
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if (!isset($this->weight) && ($groups = $storage->loadMultiple())) {
// Set a role weight to make this new role last.
      $max = array_reduce($groups, function($max, $group) {
        return $max > $group->weight ? $max : $group->weight;
      });
      $this->weight = $max + 1;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

// Clear render cache.
    entity_render_cache_clear();
  }

}
