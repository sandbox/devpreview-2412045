<?php

/**
 * @file
 * Contains \Drupal\role_group\GroupRolesForm.
 */

namespace Drupal\role_group;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the role group roles edit forms.
 */
class GroupRolesForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $form = parent::buildForm($form, $form_state);
    $form['actions']['edit'] = array(
      '#type' => 'link',
      '#title' => $this->t('Edit role group'),
      '#url' => $entity->urlInfo('edit-form'),
      '#weight' => 9
    );
    $form['actions']['delete']['#title'] = $this->t('Delete role group');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $roles = array_map(array('\Drupal\Component\Utility\String', 'checkPlain'), user_role_names(TRUE));
    $form['roles'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => $entity->getRolesId(),
      '#options' => $roles
    );
    return parent::form($form, $form_state, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    // Change the roles array to a list of enabled roles.
    // @todo: Alter the form state as the form values are directly extracted and
    //   set on the field, which throws an exception as the list requires
    //   numeric keys. Allow to override this per field. As this function is
    //   called twice, we have to prevent it from getting the array keys twice.
    if (is_string(key($form_state->getValue('roles')))) {
      $form_state->setValue('roles', array_keys(array_filter($form_state->getValue('roles'))));
    }
    return parent::buildEntity($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();
    
    $edit_link = $this->entity->link($this->t('Edit'), 'edit-roles-form');
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('Role group %label roles has been updated.', array('%label' => $entity->label())));
      $this->logger('role_group')->notice('Role group %label roles has been updated.', array('%label' => $entity->label(), 'link' => $edit_link));
    }
    else {
      drupal_set_message($this->t('Role group %label roles has been added.', array('%label' => $entity->label())));
      $this->logger('role_group')->notice('Role group %label roles has been added.', array('%label' => $entity->label(), 'link' => $edit_link));
    }
    $form_state->setRedirect('entity.role_group.collection');
  }

}
