<?php

/**
 * @file
 * Contains \Drupal\role_group\Plugin\Field\FieldFormatter\StringFormatter.
 */

namespace Drupal\role_group\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\String;
use Drupal\role_group\Entity\RoleGroup;

/**
 * Plugin implementation of the 'role_group_string' formatter.
 *
 * @FieldFormatter(
 *   id = "role_group_string",
 *   module = "role_group",
 *   label = @Translation("Plain text"),
 *   field_types = {
 *     "role_group"
 *   }
 * )
 */
class StringFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $user = \Drupal::currentUser();
    if (!$user->hasPermission('view role group fields')) {
      return array();
    }

    $elements = array();
    foreach ($items as $delta => $item) {
      // The text value has no text format assigned to it, so the user input
      // should equal the output, including newlines.
      if ($item->value && ($role = entity_load('role_group', $item->value)) && $user->hasPermission('view role group field value ' . $item->value)) {
        $value = $role->label();
        $elements[$delta] = array('#markup' => nl2br(String::checkPlain($value)));
      }
    }
    return $elements;
  }

}
