<?php

/**
 * @file
 * Contains \Drupal\role_group\Plugin\Field\FieldType\RoleGroupItem.
 */

namespace Drupal\role_group\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'role_group' field type.
 *
 * @FieldType(
 *   id = "role_group",
 *   label = @Translation("Role group"),
 *   module = "role_group",
 *   description = @Translation("This field stores a role group in the database."),
 *   default_widget = "role_group_select",
 *   default_formatter = "role_group_string"
 * )
 */
class RoleGroupItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => 256,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Text value'))
      ->addConstraint('Length', array('max' => 255))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = array(
      'role_groups' => 'all',
      'role_groups_available' => array()
      ) + parent::defaultFieldSettings();

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    $element['role_groups'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Avalible role groups'),
      '#default_value' => $settings['role_groups'],
      '#options' => array(
        'all' => $this->t('All role groups'),
        'choice' => $this->t('Choice role groups')
      ),
      '#required' => TRUE,
      '#description' => $this->t('Warning: this setting does not change the already established group of users.')
    );

    $element['role_groups_available'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Choice avalible role groups'),
      '#default_value' => $settings['role_groups_available'],
      '#options' => user_role_group_names(),
      '#states' => array(
        'visible' => array(
          ':input[name="field[settings][role_groups]"]' => array('value' => 'choice')
        ),
        'disabled' => array(
          ':input[name="field[settings][role_groups]"]' => array('value' => 'all')
        )
      ),
      '#element_validate' => array(
        array(get_class($this), 'validateGroupsAvalibleElement'),
      )
    );

    return $element;
  }

  /**
   * Validate the role group field.
   */
  public function validateGroupsAvalibleElement(array $element, FormStateInterface $form_state) {
    $form_state->setValueForElement($element, $element['#value']);
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();
    batch_set(array(
      'operations' => array(
        array('_role_group_role_group_update_user_roles', array())
      )
    ));
  }

}
