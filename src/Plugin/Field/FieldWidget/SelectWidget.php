<?php

/**
 * @file
 * Contains \Drupal\role_group\Plugin\Field\FieldWidget\SelectWidget.
 */

namespace Drupal\role_group\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'role_group_select' widget.
 *
 * @FieldWidget(
 *   id = "role_group_select",
 *   module = "role_group",
 *   label = @Translation("Select role group"),
 *   field_types = {
 *     "role_group"
 *   }
 * )
 */
class SelectWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    if (!$user->hasPermission('use role group fields')) {
      return array();
    }

    $role_groups = user_role_group_names();

    if ($this->getFieldSetting('role_groups') != 'all') {
      $role_groups_available = $this->getFieldSetting('role_groups_available');
      foreach ($role_groups as $group_id => $group) {
        if (!array_key_exists($group_id, $role_groups_available)) {
          unset($role_groups[$group_id]);
        }
      }
    }

    $user = \Drupal::currentUser();
    foreach ($role_groups as $group_id => $group) {
      if (!$user->hasPermission('assign role group ' . $group_id)) {
        unset($role_groups[$group_id]);
      }
    }

    $role_groups = array_merge(array('' => $this->t('None')), $role_groups);

    $element += array(
      '#type' => 'select',
      '#options' => $role_groups,
      '#element_validate' => array(
        array($this, 'validateElement'),
      )
    );
    if (isset($items[$delta]->value)) {
      $element['#default_value'] = $items[$delta]->value;
    }
    return array('value' => $element);
  }

  /**
   * Validate the color text field.
   */
  public function validateElement($element, FormStateInterface $form_state) {
    $parents = $element['#parents'];
    $value = array_pop($parents);
    if (empty($form_state->getValue($parents)[$value])) {
      $form_state->unsetValue($parents);
    }
  }

}
