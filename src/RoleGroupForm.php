<?php

/**
 * @file
 * Contains \Drupal\role_group\RoleGroupForm.
 */

namespace Drupal\role_group;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the role group entity edit forms.
 */
class RoleGroupForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $form = parent::buildForm($form, $form_state);
    if ($entity->id()) {
      $form['actions']['edit_roles'] = array(
        '#type' => 'link',
        '#title' => $this->t('Edit group roles'),
        '#url' => $entity->urlInfo('edit-roles-form'),
        '#weight' => 9
      );
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Role group name'),
      '#default_value' => $entity->label(),
      '#size' => 30,
      '#required' => TRUE,
      '#maxlength' => 64,
      '#description' => $this->t('The name for this role group. Example: "Moderator", "Editorial board", "Site architect".'),
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#required' => TRUE,
      '#disabled' => !$entity->isNew(),
      '#size' => 30,
      '#maxlength' => 64,
      '#machine_name' => array(
        'exists' => ['\Drupal\role_group\Entity\RoleGroup', 'load'],
      ),
    );
    $form['weight'] = array(
      '#type' => 'value',
      '#value' => $entity->getWeight(),
    );

    return parent::form($form, $form_state, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Prevent leading and trailing spaces in role names.
    $entity->set('label', trim($entity->label()));
    $status = $entity->save();

    $edit_link = $this->entity->link($this->t('Edit'));
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('Role group %label has been updated.', array('%label' => $entity->label())));
      $this->logger('role_group')->notice('Role group %label has been updated.', array('%label' => $entity->label(), 'link' => $edit_link));
      $form_state->setRedirect('entity.role_group.collection');
    }
    else {
      drupal_set_message($this->t('Role group %label has been added.', array('%label' => $entity->label())));
      $this->logger('role_group')->notice('Role group %label has been added.', array('%label' => $entity->label(), 'link' => $edit_link));
      $form_state->setRedirect('entity.role_group.edit_roles_form', array('role_group' => $entity->id()));
    }
  }

}
