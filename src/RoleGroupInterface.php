<?php

/**
 * @file
 * Contains \Drupal\role_group\RoleGroupInterface.
 */

namespace Drupal\role_group;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface RoleGroupInterface extends ConfigEntityInterface {

  /**
   * Returns the weight.
   *
   * @return int
   *   The weight of this role group.
   */
  public function getWeight();

  /**
   * Sets the weight to the given value.
   *
   * @param int $weight
   *   The desired weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * {@inheritdoc}
   */
  public function addRole($role);

  /**
   * {@inheritdoc}
   */
  public function hasRole($role);

  /**
   * {@inheritdoc}
   */
  public function delRole($role);

  /**
   * {@inheritdoc}
   */
  public function getRoles();

  /**
   * {@inheritdoc}
   */
  public function getRolesId();
}
