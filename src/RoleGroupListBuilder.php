<?php

/**
 * @file
 * Contains \Drupal\role_group\RoleGroupListBuilder.
 */

namespace Drupal\role_group;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a class to build a listing of user role group entities.
 *
 * @see \Drupal\role_group\Entity\RoleGroup
 */
class RoleGroupListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_admin_role_groups_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = t('Name');
    $header['roles'] = array(
      'data' => $this->t('Roles'),
      'class' => array(RESPONSIVE_PRIORITY_LOW),
    );
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $this->getLabel($entity);
    $row['roles'] = array();
    $roles = array();
    foreach ($entity->getRoles() as $role) {
      $roles[$role->id()] = $role->label();
    }
    asort($roles);
    $row['roles']['data'] = array(
      '#theme' => 'item_list',
      '#items' => $roles,
    );
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if ($entity->hasLinkTemplate('edit-roles-form')) {
      $operations['roles'] = array(
        'title' => t('Edit roles'),
        'weight' => 20,
        'url' => $entity->urlInfo('edit-roles-form'),
      );
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    drupal_set_message(t('The role groups settings have been updated.'));
  }

}
