<?php

/**
 * @file
 * Contains \Drupal\role_group\RoleGroupPermissions.
 */

namespace Drupal\role_group;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a class containing permission callbacks.
 */
class RoleGroupPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of role group permissions.
   *
   * @return array
   */
  public function groupPermissions() {
    $groups = user_role_group_names();
    $permissions = array();
    foreach ($groups as $group_id => $group) {
      $permissions['assign role group ' . $group_id] = array(
        'title' => $this->t('Assign %group role group', array('%group' => $group)),
        'description' => $this->t('To assign a role group must have permission "Use role group fields"')
      );
      $permissions['view role group field value ' . $group_id] = array(
        'title' => $this->t('View %group role group field value', array('%group' => $group)),
        'description' => $this->t('To view a role group field value must have permission "View role group fields"')
      );
    }
    return $permissions;
  }

}
